﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Factories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Services
{
    public static class ServicesModule
    {
        public static void AddServicesModule(this IServiceCollection services)
        {
            services.AddScoped<IPartnerLimitService, PartnerLimitService>();

            services.AddFactoriesModule();
        }
    }
}
