﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Factories
{
    public static class FactoriesModule
    {
        public static void AddFactoriesModule(this IServiceCollection services)
        {
            services.AddScoped<IPartnerLimitFactory, PartnerLimitFactory>();
        }
    }
}
