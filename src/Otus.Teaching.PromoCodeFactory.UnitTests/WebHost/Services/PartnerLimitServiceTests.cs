﻿using System;
using Xunit;

using Moq;
using AutoFixture.AutoMoq;
using AutoFixture;
using FluentAssertions;
using Shouldly;

using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services;
using Otus.Teaching.PromoCodeFactory.Factories;
using Otus.Teaching.PromoCodeFactory.UnitTests.Factories;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Services
{
    public class PartnerLimitServiceTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IPartnerLimitFactory> _limitFactoryMock;
        private readonly PartnerLimitService _partnerLimitService;

        public PartnerLimitServiceTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _limitFactoryMock = new Mock<IPartnerLimitFactory>();
            _fixture.Inject(_limitFactoryMock);
            _partnerLimitService = _fixture.Build<PartnerLimitService>().OmitAutoProperties().Create();
        }

        [Fact]
        public void PartnerLimitService_ResetPartnerLimit_NumberIssuedReset()
        {
            // Arrange
            var partner = PartnerFactory.CreatePartner(2, 5);
            var limit = partner.PartnerLimits.SingleOrDefault(pl => !pl.CancelDate.HasValue);

            _limitFactoryMock.Setup(rep => rep.Create(It.IsAny<Partner>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(limit);

            // Act
            _partnerLimitService.ResetPartnerLimit(partner);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public void PartnerLimitService_ResetPartnerLimit_OldLimitCancelled()
        {
            // Arrange
            var partner = PartnerFactory.CreatePartner(2, 5);
            var oldLimit = partner.PartnerLimits.SingleOrDefault(pl => !pl.CancelDate.HasValue);
            var newLimit = LimitFactory.CreateActive(partner);

            _limitFactoryMock.Setup(rep => rep.Create(It.IsAny<Partner>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(newLimit);

            // Act
            _partnerLimitService.ResetPartnerLimit(partner);

            // Assert
            oldLimit.CancelDate.HasValue.Should().BeTrue();
        }

        [Fact]
        public void PartnerLimitService_ResetPartnerHitLimit_NumberIssuedNotReset()
        {
            // Arrange
            var partner = PartnerFactory.CreatePartnerWithHitLimit();
            var limit = partner.PartnerLimits.SingleOrDefault(pl => !pl.CancelDate.HasValue);

            // Act
            _partnerLimitService.ResetPartnerLimit(partner);

            // Assert
            partner.NumberIssuedPromoCodes.Should().BeGreaterThan(0);
        }

        [Fact]
        public void PartnerLimitService_CreatePartnerLimit_ZeroLimitExceptionThrown()
        {
            // Arrange
            var partner = PartnerFactory.CreatePartnerWithNotHitLimit();
            var limit = partner.PartnerLimits.SingleOrDefault(pl => !pl.CancelDate.HasValue);

            _limitFactoryMock.Setup(rep => rep.Create(It.IsAny<Partner>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(limit);

            // Act
            var exception = Should.Throw(() => _partnerLimitService.CreatePartnerLimit(partner, 0, DateTime.MaxValue), typeof(ArgumentException));
            
            // Assert
            exception.Message.Should().Be("Лимит должен быть больше 0");
        }

        [Fact]
        public void PartnerLimitService_CreatePartnerLimit_LimitCreated()
        {
            // Arrange
            var partner = PartnerFactory.CreatePartner(2, 5);
            var limit = LimitFactory.CreateActive(partner);

            _limitFactoryMock.Setup(rep => rep.Create(It.IsAny<Partner>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(limit);

            // Act
            _partnerLimitService.CreatePartnerLimit(partner, 5, DateTime.MaxValue);

            // Assert
            _limitFactoryMock.Verify(m => m.Create(It.IsAny<Partner>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()), Times.Once());
            partner.PartnerLimits.Should().Contain(limit);
        }
    }
}
