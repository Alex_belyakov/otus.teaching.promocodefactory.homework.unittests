﻿using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;

using Moq;
using AutoFixture.AutoMoq;
using AutoFixture;
using FluentAssertions;

using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Common;
using Otus.Teaching.PromoCodeFactory.Factories;
using Otus.Teaching.PromoCodeFactory.UnitTests.Factories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Repositories
{
    public class PartnerRepositoryTests : IClassFixture<TestFixtureInMemory>
    {
        private readonly IRepository<Partner> _partnerRepository;

        public PartnerRepositoryTests(TestFixtureInMemory testFixture)
        {
            var serviceProvider = testFixture.ServiceProvider;
             _partnerRepository = serviceProvider.GetService<IRepository<Partner>>();
        }

        [Fact]
        public async void PartnerRepository_Create_PartnerCreated()
        {
            // Arrange
            var partner = PartnerFactory.CreatePartnerWithNotHitLimit();

            // Act
            await _partnerRepository.AddAsync(partner);
            var addedPartner = await _partnerRepository.GetByIdAsync(partner.Id);

            // Assert
            addedPartner.Should().NotBeNull();
            addedPartner.Id.Should().Be(partner.Id);
        }

        [Fact]
        public async void PartnerRepository_Update_LimitUpdated()
        {
            // Arrange
            var partner = PartnerFactory.CreatePartner(0, 10);

            await _partnerRepository.AddAsync(partner);

            var addedPartner = await _partnerRepository.GetByIdAsync(partner.Id);
            var newLimit = LimitFactory.CreateActive(addedPartner);
            addedPartner.PartnerLimits.Add(newLimit);

            // Act
            await _partnerRepository.UpdateAsync(addedPartner);
            var updatedPartner = await _partnerRepository.GetByIdAsync(partner.Id);

            // Assert
            updatedPartner.Should().NotBeNull();
            updatedPartner.PartnerLimits.Should().Contain(newLimit);
        }
    }
}
