﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Module;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Common
{
    public class TestFixtureInMemory : IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }

        /// <summary>
        /// Выполняется перед запуском тестов
        /// </summary>
        public TestFixtureInMemory()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddInMemoryDbContext();
            serviceCollection.AddCommonModule();
            var serviceProvider = serviceCollection
                .BuildServiceProvider();
            ServiceProvider = serviceProvider;
        }

        public void Dispose()
        {
        }
    }
}
